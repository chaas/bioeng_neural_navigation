# Drosophila Neural Velocity Estimation Computational Model

## About
This repository includes the source code for the drosophila neural velocity estimation computational model implementation described in the report: 

A COMPUTATIONAL MODEL OF THE DROSOPHILA NEURAL VELOCITY ESTIMATION CIRCUIT

Submitted for academic credit in the Controlling Behavior in Animals and Robots course at the EPFL in spring 2024. 

The model and the report are based principally on the work of Lu et al. in the following article. 

 >J. Lu, A. H. Behbahani, L. Hamburg et al., 'Transforming representations of movement from body- to world-centric space,' eng, Nature (London), vol. 601, no. 7891, pp. 98-104, 2022, ISSN: 0028-0836.

 The connectivity weighting matrices used in this model are copied from Lu et al. from the original repository provided with their paper here: 

 https://github.com/druckmann-lab/Translational-velocity-and-heading-model

 ## Quickstart 

 To test the neural computational model, clone the repository: 

 ```
 git clone git@gitlab.epfl.ch:chaas/bioeng_neural_navigation.git
 ```

 install the [flygym](https://github.com/NeLy-EPFL/flygym) environment with

 ```
 pip install "flygym"
 ``` 

 The model can then be tested with abstract inputs by running the `test_navigator.py` script. 

 To simulate a fly walking scenario in FlyGym and apply the navigational model, run the `scenario.ipynb` notebook.