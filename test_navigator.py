#####################################################################
# Test stubs for the neural navigator class and member functions    #
#####################################################################
from navigator import NeuralNavigationModel
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import numpy as np
import time
import os

current_directory = os.getcwd()

Model = NeuralNavigationModel(None, None, None, None, None)


def load_csv_to_array(file_path):
    data = np.loadtxt(file_path, delimiter=",", dtype=int)
    return data


def test_1_gaussian_activation():
    """test the gaussian activation function"""
    # test multiple values sampled around a single mean
    mean = 0
    cov = 1
    scale = 1
    test_val = np.linspace(-10, 10, 40)

    output = Model.gaussian_activation(mean, cov, scale, test_val)

    plt.scatter(test_val, output)
    plt.xlabel("test value")
    plt.ylabel("activation")
    plt.title("Gaussian activations - single mean")
    plt.show()

    # test one value sampled for many means
    mean = np.linspace(-10, 10, 40)
    test_val = 0

    output = Model.gaussian_activation(mean, cov, scale, test_val)

    plt.scatter(mean, output)
    plt.xlabel("distribution mean")
    plt.ylabel("activation")
    plt.title("Gaussian activations - multiple means, test value at 0")
    plt.show()


def test_2_population_activity_heading():
    """test the population activity heading function
    expected results: an orientation input should excite neurons tuned to
                      similar directions according to their gaussian activation
                      functions
    """

    # Generate population parameters
    pop_size = 10
    theta_preferred = np.linspace(0, 2 * np.pi, pop_size)
    activation_params = {"cov": 1, "scale": 1}

    # test values
    theta_values = np.linspace(0, 2 * np.pi, 10)

    # Initialize an empty list to store activity values
    activity_values = []

    # Calculate activity for each theta value and store in the list
    for theta in theta_values:
        activity = Model.population_activity_heading(
            theta, theta_preferred, activation_params
        )
        activity_values.append(activity)

    # Create a figure and axis
    fig, ax = plt.subplots()

    # Create an empty bar plot
    bars = ax.bar(theta_preferred * 180 / np.pi, activity_values[0], width=10)

    # Function to update the plot for each frame of the animation
    def update(frame):
        # Update the heights of the bars with new activity values
        for bar, height in zip(bars, activity_values[frame]):
            bar.set_height(height)
            ax.set_title(
                "Population activation for orientation: "
                + str(np.round(theta_values[frame] * 180 / np.pi, 1))
            )
        return bars

    # Create an animation
    ani = FuncAnimation(
        fig, update, frames=len(activity_values), interval=1000, repeat=False
    )

    ax.set_xlabel("Preferred Direction")
    ax.set_ylabel("Activity Level")

    # Show the plot
    plt.show()


def test_3_population_activity_heading_direction():
    """test the population activity function
       expected results: the neuron population should be excited proportionate to the
                         similarity in heading and translational direction to the preferred
                         values of each neuron
    """

    # Generate population parameters
    pop_size = 10
    theta_preferred = np.linspace(0, 2 * np.pi, pop_size)
    phi_preferred = np.array([np.sqrt(2) / 2, np.sqrt(2) / 2])  # 45 degrees
    activation_params = {"cov": 1, "scale": 1}

    # test values
    theta_values = np.linspace(0, 2 * np.pi, 10)
    displacement_values = [
        np.array([np.cos(a), np.sin(a)]) for a in np.linspace(0, 2 * np.pi, 9)
    ]

    # Initialize an empty list to store activity values
    activity_values = []

    # Calculate activity for each theta value and store in the list
    for displacement in displacement_values:
        for theta in theta_values:
            activity = Model.population_activity(
                displacement, theta, phi_preferred, theta_preferred, activation_params
            )
            activity_values.append(activity)

    # Create a figure and axis
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 6))

    # Create an empty bar plot
    bars = ax1.bar(theta_preferred * 180 / np.pi, activity_values[0], width=10)
    ax1.set_xlabel("Preferred Direction")
    ax1.set_ylabel("Activity Level")

    # Plot displacement values in the second subplot
    (displacement_line,) = ax2.plot([], [], label="Displacement Vector (body-centric)")
    (displacement_point,) = ax2.plot([], [], "ro")
    (preferred_direction_line,) = ax2.plot(
        [], [], label="Preferred displacement Vector (body-centric)", color="r"
    )
    (preferred_direction_point,) = ax2.plot([], [], "ro")
    ax2.set_xlim(-1.5, 1.5)
    ax2.set_ylim(-1.5, 1.5)
    ax2.set_xlabel("X")
    ax2.set_ylabel("Y")
    ax2.set_title("Displacement Vector (body-centric)")

    # Function to update the plot for each frame of the animation
    def update(frame):
        # Update the heights of the bars with new activity values
        for bar, height in zip(bars, activity_values[frame]):
            bar.set_height(height)
            ax1.set_title(
                "Population activation for orientation: "
                + str(
                    np.round(theta_values[frame % len(theta_values)] * 180 / np.pi, 1)
                )
            )

        # Update displacement values plot
        displacement = displacement_values[frame // len(theta_values)]
        displacement_line.set_data([0, displacement[0]], [0, displacement[1]])
        displacement_point.set_data(displacement[0], displacement[1])

        preferred_direction_line.set_data([0, phi_preferred[0]], [0, phi_preferred[1]])
        preferred_direction_point.set_data(displacement[0], displacement[1])

        return bars, displacement_line, displacement_point

    # Create an animation
    ani = FuncAnimation(
        fig, update, frames=len(activity_values), interval=1000, repeat=False
    )

    # Show the plot
    plt.tight_layout()
    plt.show()


def test_4_navigator_population_activation_layer():
    """test the population layer as a whole
       expected results: the neuron populations activities should reflect the orientation and direction
                         of travel of the fly, with visualization
    """

    ## Configure the model
    weights = None  # list of weight matrices [PFNdl, PFNdr, PFNvl, PFNvr]
    pop_size = 4
    preferred_orientations = [
        np.linspace(0, 2 * np.pi, pop_size) for _ in range(4)
    ]  # list of one dimensional preferred orientations (in rad in the world frame) for each population [PFNdl, PFNdr, PFNvl, PFNvr]
    preferred_directions = [
        np.array([np.cos(np.pi / 4), np.sin(np.pi / 4)]),  # PFNdl
        np.array([np.cos(7 * 2 * np.pi / 8), np.sin(7 * 2 * np.pi / 8)]),  # PFNDr
        np.array([np.cos(5 * 2 * np.pi / 8), np.sin(5 * 2 * np.pi / 8)]),  # PFNvl
        np.array([np.cos(3 * 2 * np.pi / 8), np.sin(3 * 2 * np.pi / 8)]),  # PFNvr
    ]  # list of preferred directions (2D unit vectors in the body frame) for each population [PFNdl, PFNdr, PFNvl, PFNvr]

    test_model = NeuralNavigationModel(
        weights=weights,
        preferred_directions=preferred_directions,
        preferred_orientations=preferred_orientations,
    )

    ## Test implementation
    # test values
    theta_values = np.linspace(0, 2 * np.pi, 10)
    displacement_values = [
        np.array([np.cos(a), np.sin(a)]) for a in np.linspace(0, 2 * np.pi, 18)
    ]

    test_model.init_PFN_activities_recording([0, 1, 2, 3])

    for theta in theta_values:
        for displacement in displacement_values:
            test_model.update_PFN_activity(displacement, theta)
            test_model.log_state()

    test_model.show_PFN_animations(interval=1000)


def test_5_navigator_forward_pass():
    """test the forward pass of the navigational model (with unrealistic encodings)
       expected results: accurate model tracking of the ground-truth world-centric direction
    """

    ## Configure the model
    weights = [
        np.array([[1, 0, 0, 1], [1, 1, 0, 0], [0, 1, 1, 0], [0, 0, 1, 1]]),
        np.array([[1, 1, 0, 0], [0, 1, 1, 0], [0, 0, 1, 1], [1, 0, 0, 1]]),
        np.array([[0, 1, 1, 0], [0, 0, 1, 1], [1, 0, 0, 1], [1, 1, 0, 1]]),
        np.array([[0, 0, 1, 1], [1, 0, 0, 1], [1, 1, 0, 0], [0, 1, 1, 0]]),
    ]  # list of weight matrices [PFNdl, PFNdr, PFNvl, PFNvr]
    pop_size = 4
    preferred_orientations = [
        np.linspace(0, 2 * np.pi, pop_size, endpoint=False) for _ in range(4)
    ]  # list of one dimensional preferred orientations (in rad in the world frame) for each population [PFNdl, PFNdr, PFNvl, PFNvr]
    preferred_directions = [
        np.array([np.cos(np.pi / 4), np.sin(np.pi / 4)]),  # PFNdl
        np.array([np.cos(7 * 2 * np.pi / 8), np.sin(7 * 2 * np.pi / 8)]),  # PFNDr
        np.array([np.cos(5 * 2 * np.pi / 8), np.sin(5 * 2 * np.pi / 8)]),  # PFNvl
        np.array([np.cos(3 * 2 * np.pi / 8), np.sin(3 * 2 * np.pi / 8)]),  # PFNvr
    ]  # list of preferred directions (2D unit vectors in the body frame) for each population [PFNdl, PFNdr, PFNvl, PFNvr]

    test_model = NeuralNavigationModel(
        weights=weights,
        preferred_directions=preferred_directions,
        preferred_orientations=preferred_orientations,
        realistic_mode=False
    )

    ## Test implementation
    # test values
    theta_values = np.linspace(0, 2 * np.pi, 9)
    displacement_values = [
        np.array([np.cos(a), np.sin(a)]) for a in np.linspace(0, 2 * np.pi, 9)
    ]

    test_model.init_PFN_activities_recording([0, 1, 2, 3])
    test_model.init_hDeltaB_activities_recording()

    for theta in theta_values:
        for displacement in displacement_values:
            test_model.forward(displacement, theta)
            test_model.decode()
            test_model.log_state()

    # test_model.show_PFN_animations(interval=1000)
    # test_model.show_hDeltaB_animation1(interval=1000)
    test_model.show_hDeltaB_animation2(interval=2000)


def test_6_navigator_real_dimensions(ablate_D_matrices: bool = False, ablate_W_matrices: bool = False):
    """test the forward pass of the navigational model (with unrealistic encodings),
       implement ablation tests for different parts of the model
       expected results: accurate model tracking of the ground-truth world-centric direction
    """

    W_PFNdL_hDb = load_csv_to_array(current_directory + "\connectivity\PFNdL_hDb.csv")
    W_PFNdR_hDb = load_csv_to_array(current_directory + "\connectivity\PFNdR_hDb.csv")
    W_PFNvL_hDb = load_csv_to_array(current_directory + "\connectivity\PFNvL_hDb.csv")
    W_PFNvR_hDb = load_csv_to_array(current_directory + "\connectivity\PFNvR_hDb.csv")

    # normalize weight matrices along each row, weight the PFNd inputs higher
    W_PFNdL_hDb = 6.0*np.divide(W_PFNdL_hDb, np.sum(W_PFNdL_hDb, 1)[:,None])
    W_PFNdR_hDb = 6.0*np.divide(W_PFNdR_hDb, np.sum(W_PFNdR_hDb, 1)[:,None])
    W_PFNvL_hDb = np.divide(W_PFNvL_hDb, np.sum(W_PFNvL_hDb, 1)[:,None])
    W_PFNvR_hDb = np.divide(W_PFNvR_hDb, np.sum(W_PFNvR_hDb, 1)[:,None])

    # Ablation test - disrupt w-matrices
    if ablate_W_matrices:
        W_PFNdL_hDb = np.random.random_sample(W_PFNdL_hDb.shape)
        W_PFNdR_hDb = np.random.random_sample(W_PFNdR_hDb.shape)
        W_PFNvL_hDb = np.random.random_sample(W_PFNvL_hDb.shape)
        W_PFNvR_hDb = np.random.random_sample(W_PFNvR_hDb.shape)
    
    weights = [W_PFNdL_hDb, np.roll(W_PFNdR_hDb,-5), W_PFNvL_hDb, W_PFNvR_hDb] # LR shift in W_PFNdR_hDb
    
    EPGL_PFNDL = load_csv_to_array(current_directory + "\connectivity\EPGL_PFNdL.csv")
    EPGR_PFNDR = load_csv_to_array(current_directory + "\connectivity\EPGR_PFNdR.csv")
    EPGL_PFNVL = load_csv_to_array(current_directory + "\connectivity\EPGL_PFNvL.csv")
    EPGR_PFNVR = load_csv_to_array(current_directory + "\connectivity\EPGR_PFNvR.csv")
    
    if ablate_D_matrices: 
        EPGL_PFNDL = np.ones(EPGL_PFNDL.shape)
        EPGR_PFNDR = np.ones(EPGR_PFNDR.shape)
        EPGL_PFNVL = np.ones(EPGL_PFNVL.shape)
        EPGR_PFNVR = np.ones(EPGR_PFNVR.shape)
    
    epg_pfn_mappings = [EPGL_PFNDL, EPGR_PFNDR, EPGL_PFNVL, EPGR_PFNVR]

    preferred_orientations_PFNdL = np.array(
        [
            180, #-180,
            180, #-180,
            225, #-135,
            225, #-135,
            270, #-90,
            270, #-90,
            315, #-45,
            315, #-45,
            315, #-45,
            315, #-45,
            0,
            0,
            45,
            45,
            45,
            45,
            90,
            90,
            135,
            135,
        ]
    )*np.pi/180
    preferred_orientations_PFNdR = np.flip(np.array(
        [
            180, #-180,
            180, #-180,
            225, #-135,
            225, #-135,
            270, #-90,
            270, #-90,
            315, #-45,
            315, #-45,
            315, #-45,
            315, #-45,
            0,
            0,
            45,
            45,
            45,
            45,
            90,
            90,
            135,
            135,
        ]
    ))*np.pi/180
    preferred_orientations_PFNdR = np.concatenate((np.array([np.pi,np.pi]),preferred_orientations_PFNdR[:-2]))
    
    preferred_orientations_PFNvL = np.array(
        [
            180, #-180,
            225, #-135,
            270, #-90,
            315, #-45,
            315, #-45,
            0,
            45,
            45,
            90,
            135
        ]
    )*np.pi/180
    preferred_orientations_PFNvR = np.flip(np.array(
        [
            180, #-180,
            225, #-135,
            270, #-90,
            315, #-45,
            315, #-45,
            0,
            45,
            45,
            90,
            135
        ]
    ))*np.pi/180
    preferred_orientations_PFNvR = np.concatenate((np.array([np.pi]),preferred_orientations_PFNvR[:-1]))

    # list of one dimensional preferred orientations (in rad in the world frame) for each population [PFNdl, PFNdr, PFNvl, PFNvr]
    preferred_orientations = [preferred_orientations_PFNdL, preferred_orientations_PFNdR, preferred_orientations_PFNvL, preferred_orientations_PFNvR]

    preferred_directions = [
        np.array([np.cos(31*np.pi/180), np.sin(31*np.pi/180)]),  # PFNdl
        np.array([np.cos(-31*np.pi/180), np.sin(-31*np.pi/180)]),  # PFNDr
        np.array([np.cos(137*np.pi/180), np.sin(137*np.pi/180)]),  # PFNvl
        np.array([np.cos(-137*np.pi/180), np.sin(-137*np.pi/180)]),  # PFNvr
    ]  # list of preferred directions (2D unit vectors in the body frame) for each population [PFNdl, PFNdr, PFNvl, PFNvr]
    
    hDeltaB_remapping = [
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
        ]

    test_model = NeuralNavigationModel(
        weights=weights,
        preferred_directions=preferred_directions,
        preferred_orientations=preferred_orientations,
        hDeltaB_remapping=hDeltaB_remapping,
        epg_pfn_mappings=epg_pfn_mappings,
        realistic_mode=True
    )

    ## Test implementation
    # test values
    theta_values = np.linspace(0, np.pi*2, 8, endpoint=False)
    displacement_values = [
        np.array([np.cos(a), np.sin(a)]) for a in np.linspace(0, np.pi*2, 8, endpoint=False)
    ]

    test_model.init_PB_activities_recording()
    test_model.init_PFN_activities_recording([0, 1, 2, 3])
    test_model.init_hDeltaB_activities_recording()

    for theta in theta_values:
        for displacement in displacement_values:
            test_model.forward(displacement, theta)
            test_model.decode()
            test_model.log_state()

    # uncomment animations to generate (only one animation can be generated per run)
    # test_model.show_PB_animations(interval=500)
    # test_model.show_PFN_animations(interval=500)
    test_model.show_hDeltaB_animation2(interval=500)
    test_model.show_angle_evolutions()


if __name__ == "__main__":

    test_6_navigator_real_dimensions(
        ablate_D_matrices=False, 
        ablate_W_matrices=False
    )
