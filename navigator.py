import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation, FFMpegWriter
import copy


class NeuralNavigationModel:
    """Computational direction estimation model based on Drosophila navigational 
       neural circuits 
       
       See the report 'A COMPUTATIONAL MODEL OF THE DROSOPHILA NEURAL VELOCITY ESTIMATION
       CIRCUIT' for a detailed description
       
       The model is based on the implementation from Lu et al.: 
       J. Lu, A. H. Behbahani, L. Hamburg et al., 'Transforming representations of movement from body- to
       world-centric space,' eng, Nature (London), vol. 601, no. 7891, pp. 98-104, 2022, ISSN: 0028-0836.
       
       Their version of the model is available here: 
       https://github.com/druckmann-lab/Translational-velocity-and-heading-model

    """

    def __init__(
        self,
        weights: list | None,
        preferred_directions: list | None,
        preferred_orientations: list | None,
        hDeltaB_remapping: list | None = None,
        epg_pfn_mappings: list | None = None,
        realistic_mode: bool = False,
    ):
        self.weights = weights                                  # list of weight matrices [PFNdl, PFNdr, PFNvl, PFNvr]
        self.preferred_orientations = preferred_orientations    # list of one dimensional preferred orientations (in rad in the world frame) for each population [PFNdl, PFNdr, PFNvl, PFNvr]
        self.preferred_directions = preferred_directions        # list of preferred directions (2D unit vectors in the body frame) for each population [PFNdl, PFNdr, PFNvl, PFNvr] (set only for realistic = False, otherwise inferred from EPFL mappings)
        self.epg_pfn_mappings = epg_pfn_mappings                # matrices mapping hemi-brain EPG to PFN connections [EPGL_PFNDL, EPGR_PFNDR, EPGL_PFNVL, EPGR_PFNVR]

        # unrealistic mode heading sensitivity params
        self.gaussian_activation_params = {"cov": 1, "scale": 1}

        # model state
        self.state_heading = [0, 0]  # instantaneous heading
        self.state_direction = [
            0,
            0,
        ]  # instantaneous body-centric translational direction
        self.state_world_direction = [
            0,
            0,
        ]  # ground truth instantaneous world-centric motion direction
        self.state_PBR_activity = np.zeros(8)
        self.state_PBL_activity = np.zeros(8)
        self.state_PFN_activities = (
            [np.zeros(dir.shape) for dir in preferred_orientations]
            if preferred_orientations is not None
            else 0
        )  # PFN population activities
        self.state_hDeltaB_activities = (
            np.zeros(weights[0].shape[0]) if weights is not None else 0
        )  # hDeltaB population activities
        self.hDeltaB_world_directions = (
            np.linspace(0, 2 * np.pi, weights[0].shape[0], endpoint=False)
            if weights is not None
            else 0
        )  # for unrealistic mode - hDeltaB encoding linearly spans 0-360 degrees
        self.state_hDeltaB_world_vectors = None         # world-centric frame encoding of hDeltaB activations as directional vectors

        self.hDeltaB_reordering = hDeltaB_remapping     # remapping of hDeltaB activity vector (for convenience - should swap hemi-brain segments)
        self.rearranged_hDeltaB = np.zeros(
            14
        )  # remapped hDeltaB activities into circular FB geometry representation
        self.state_decoded_angle = (
            0  # world-centric velocity direction decoded from the hDeltaB bump position
        )

        # logs
        self.decoded_angle_log = []
        self.gt_angle_log = []
        self.decoded_error_log = []
        
        self.PBL_log = []
        self.PBR_log = []
        self.frames = []
        self.pFN_activities_log = []
        self.heading_log = []
        self.direction_log = []
        self.world_direction_log = []
        self.hDeltaB_activities_log = []
        self.rearranged_hDeltaB_log = []
        self.hDbframes = []
        self.hDeltaB_vectors_log = []

        # mode
        self.realistic_mode = realistic_mode

    def forward(self, v_b: np.ndarray, theta: float):
        """calculate the output of the navigational model

        Args:
            v_b (numpy.ndarray): 2x1 velocity vector expressing the fly's translational velocity in it's body frame
            theta (float): angle defining the orientation of the fly in the world frame
        """

        # Get the PFN population activity
        if not self.realistic_mode:
            self.update_PFN_activity(v_b, theta)  # (updates self.state_PFN_activities)
        else:
            self.update_PFN_activity_realistic(
                v_b, theta
            )  # (updates self.state_PFN_activities)

        # Get the hDeltaB population activity contribution from each population
        activity_mappings = []
        for i, w in enumerate(self.weights):
            activity_mappings.append(np.matmul(w, self.state_PFN_activities[i]))

        # calculate the aggregate hDeltaB activity based on the contributions from each PFN population
        self.state_hDeltaB_activities = np.sum(np.array(activity_mappings), axis=0)

    def decode(self):
        """decode the navigation network output to get an estimate of the fly translational velocity in
        in the world frame given the activities of the h_delta_b neurons at the output of the network

        for realistic mode, this is done by mapping the hDeltaB neuron indices to their actual angular
        positions in the FB and decoding based on the location of the simulated "bump"

        for normal mode, this is done by taking the vector sum of the directly encoded direction vectors
        represented by each hDeltaB neuron
        """

        self.rearranged_hDeltaB = np.zeros(14)

        if self.realistic_mode:

            # map the hDeltaB neuron activation vector to the FB
            self.rearranged_hDeltaB[0:3] = self.state_hDeltaB_activities[
                self.hDeltaB_reordering
            ][0:3]
            self.rearranged_hDeltaB[3] = np.mean(
                self.state_hDeltaB_activities[self.hDeltaB_reordering][3:5]
            )
            self.rearranged_hDeltaB[4] = np.mean(
                self.state_hDeltaB_activities[self.hDeltaB_reordering][5:7]
            )
            self.rearranged_hDeltaB[5:9] = self.state_hDeltaB_activities[
                self.hDeltaB_reordering
            ][7:11]
            self.rearranged_hDeltaB[9] = np.mean(
                self.state_hDeltaB_activities[self.hDeltaB_reordering][11:14]
            )
            self.rearranged_hDeltaB[10] = np.mean(
                self.state_hDeltaB_activities[self.hDeltaB_reordering][14:16]
            )
            self.rearranged_hDeltaB[11:] = self.state_hDeltaB_activities[
                self.hDeltaB_reordering
            ][16:]

            # apply offset to encoded angles
            # (this was empirically determined through regression)
            b = -25 
            pva_shift = np.radians(b)

            # decode the world-centric motion angle indicated by the FB activation map
            encoding_angles = np.linspace(
                -np.pi - pva_shift, np.pi - pva_shift, 14, endpoint=False
            )
            encoding_xs = np.cos(encoding_angles)
            encoding_ys = np.sin(encoding_angles)

            decoded_x = np.dot(self.rearranged_hDeltaB, encoding_xs)
            decoded_y = np.dot(self.rearranged_hDeltaB, encoding_ys)

            self.state_decoded_angle = np.arctan2(decoded_y, decoded_x)

            # re-encode the activations as normalized vectors in the world frame
            self.state_hDeltaB_world_vectors = np.array(
                [
                    activity * np.array([np.cos(a), np.sin(a)])
                    for a, activity in zip(encoding_angles, self.rearranged_hDeltaB)
                ]
            )
            self.state_hDeltaB_world_vectors = (
                self.state_hDeltaB_world_vectors
                / np.max(self.state_hDeltaB_world_vectors)
            )

        else:

            # each neuron represents a component of the velocity in the world-centric direction it encodes
            self.state_hDeltaB_world_vectors = np.array(
                [
                    activity * np.array([np.cos(a), np.sin(a)])
                    for a, activity in zip(
                        self.hDeltaB_world_directions, self.state_hDeltaB_activities
                    )
                ]
            )

            # the actual velocity of the fly is the vector sum of the components
            hDeltaB_vector = np.sum(self.state_hDeltaB_world_vectors, axis=0)

            self.state_decoded_angle = np.arctan2(hDeltaB_vector[1], hDeltaB_vector[0])

        # log the ground-truth and encoded motion angles as well as the error
        gt_angle = np.arctan2(
            self.state_world_direction[1], self.state_world_direction[0]
        )
        self.decoded_angle_log.append(self.state_decoded_angle)
        self.gt_angle_log.append(gt_angle)
        self.decoded_error_log.append(
            self.wrap_to_pi(gt_angle - self.state_decoded_angle)
        )

        return self.state_decoded_angle

    def update_PFN_activity(self, v_b: np.ndarray, theta: float):
        """update the activity of the PFN populations given the current orientation and
           body centric travel direction, with the normal (unrealistic) method

        Args:
            v_b (np.ndarray): 2x1 velocity vector expressing the fly's translational velocity in it's body frame
            theta (float): angle defining the orientation of the fly in the world frame
        """

        for i in range(len(self.state_PFN_activities)):
            self.state_PFN_activities[i] = self.population_activity(
                v_b=v_b,
                theta=theta,
                phi_preferred=self.preferred_directions[i],
                theta_preferred=self.preferred_orientations[i],
                heading_activation_params=self.gaussian_activation_params,
            )

        # update the body translational direction and heading states as well
        self.state_direction = v_b
        self.state_heading = np.array([np.cos(theta), np.sin(theta)])
        world_angle = theta + np.arctan2(v_b[1], v_b[0])
        self.state_world_direction = np.array(
            [np.cos(world_angle), np.sin(world_angle)]
        )

    def population_activity(
        self,
        v_b: np.ndarray,
        theta: float,
        phi_preferred: np.ndarray,
        theta_preferred: np.ndarray,
        heading_activation_params: dict,
    ):
        """get the activity of a population of neurons given the body-centric translational direction, the orientation
           and the preferred direction and orientation of the population

        Args:
            v_b (np.ndarray): 2x1 velocity vector expressing the fly's translational velocity in it's body frame
            theta (float): angle defining the orientation of the fly in the world frame
            phi_preferred (np.ndarray): 2x1 directional vector indicating the preferred direction of the neuron population
            theta_preferred (np.ndarray): set of preferred directions for each neuron in the population
            activation_params (dict): dictionary with the activation function parameters for the population heading sensitivities,
                            for gaussian activations:
                            {'cov':<>, 'scale':<>}
        """

        # get the component of the translational velocity in the preferred direction of the population
        unit_pi_preferred = phi_preferred / np.linalg.norm(phi_preferred)
        v_p = np.dot(v_b, unit_pi_preferred)

        heading_activity_mask = self.population_activity_heading(
            theta, theta_preferred, heading_activation_params
        )

        # the population activity is the product of the two sensitivities
        # clipped at zero to reflect directional senstivities
        activity = np.maximum(
            v_p * heading_activity_mask, np.zeros(heading_activity_mask.shape)
        )

        return activity

    def population_activity_heading(
        self, theta: float, theta_preferred: np.ndarray, activation_params: dict
    ):
        """get the activity of a population given a heading input and the
           preferred direction of each neuron

        Args:
            theta (float): perceived heading
            theta_preferred (np.ndarray): set of preferred directions for each neuron in the population
            activation_params (dict): dictionary with the activation function parameters for the population,
                                      for gaussian activations:
                                      {'cov':<>, 'scale':<>}
        """

        # The heading activity mask value of each neuron is the activation function around that neuron's preferred
        # direction sampled at the input heading direction theta, of course it wraps around the limits at 0
        # and 360 degrees
        activity = self.gaussian_activation(
            mean=theta_preferred,
            cov=activation_params["cov"],
            scale=activation_params["scale"],
            test_val=theta,
        )

        activity_wrapped_up = self.gaussian_activation(
            mean=theta_preferred,
            cov=activation_params["cov"],
            scale=activation_params["scale"],
            test_val=theta + 2 * np.pi,
        )

        activity_wrapped_down = self.gaussian_activation(
            mean=theta_preferred,
            cov=activation_params["cov"],
            scale=activation_params["scale"],
            test_val=theta - 2 * np.pi,
        )

        return activity + activity_wrapped_up + activity_wrapped_down

    def gaussian_activation(
        self, mean: float, cov: float, scale: float, test_val: float
    ):
        """get an activation value as the value of the gaussian distribution defined
           by the mean and covariance at the test value, and scaled by the scale

        Args:
            mean (float): mean value of the test distribution
            cov (float): covariance value of the test distribution
            scale (float): scaling factor of the activation value
            test_val (float): test value at which to evaluate the activation function for the activation value
        """

        # Calculate the standard deviation from the covariance
        std_dev = np.sqrt(cov)

        # Calculate the exponent term
        exponent = -0.5 * ((test_val - mean) / std_dev) ** 2

        # Calculate the Gaussian activation value
        activation_value = scale * (1 / (np.sqrt(2 * np.pi * cov))) * np.exp(exponent)

        return activation_value

    def update_PFN_activity_realistic(self, v_b: np.ndarray, theta: float, n_discrete_EPG_angles = 8):
        """update the activity of the PFN populations given the current orientation and
           body centric travel direction, with the realistic strategy based on the EPG 
           mappings to the PFN populations

        Args:
            v_b (np.ndarray): 2x1 velocity vector expressing the fly's translational velocity in it's body frame
            theta (float): angle defining the orientation of the fly in the world frame
        """

        # get the component of the body-centric translational velocity in the direction of each 
        # PFN population's preferred direction
        v_p = []  # [PFNdl, PFNdr, PFNvl, PFNvr]
        for phi_preferred in self.preferred_directions:
            unit_pi_preferred = phi_preferred / np.linalg.norm(phi_preferred)
            v_p.append(np.dot(v_b, unit_pi_preferred))
        
        # get the discrete angles encoded by the EPG populations in each hemi-brain
        EPGR_angles = np.linspace(-np.pi, np.pi, n_discrete_EPG_angles, endpoint=False)
        EPGL_angles = np.linspace(np.pi, -np.pi, n_discrete_EPG_angles, endpoint=False)

        # get a baseline activity profile for each population
        # this is implicitly binning all the EPG neurons in each half of the PB
        # giving bumps for the discrete directions captured by that set
        PBr_activity_base = self.epg_activity_profile(EPGR_angles, shape_param=0.25)
        PBl_activity_base = self.epg_activity_profile(EPGL_angles, shape_param=0.25)

        # create a simulated "bump" at the part of each PB encoding the heading angle
        # reverse heading map for right EPG set
        PBr_activity = self.align_profile(PBr_activity_base, -theta)
        PBl_activity = self.align_profile(PBl_activity_base, theta)
        
        # update the states tracking the bump in the PB
        self.state_PBL_activity = PBl_activity
        self.state_PBR_activity = PBr_activity

        # get the heading map for each PFN population based on their connections
        # mapping from the PB and the EPG neurons therein 
        PFNd_l_heading_map = np.matmul(self.epg_pfn_mappings[0], PBl_activity)
        PFNd_r_heading_map = np.matmul(self.epg_pfn_mappings[1], PBr_activity)
        PFNv_l_heading_map = np.matmul(self.epg_pfn_mappings[2], PBl_activity)
        PFNv_r_heading_map = np.matmul(self.epg_pfn_mappings[3], PBr_activity)

        # scale the heading map activity inherited from the EPG neurons in the PB
        # by the alignment of the translational direction with the population's
        # preferred direction
        PFND_L_activity = v_p[0] * PFNd_l_heading_map
        PFND_R_activity = v_p[1] * PFNd_r_heading_map
        PFNV_L_activity = v_p[2] * PFNv_l_heading_map
        PFNV_R_activity = v_p[3] * PFNv_r_heading_map

        self.state_PFN_activities = [PFND_L_activity, PFND_R_activity, PFNV_L_activity, PFNV_R_activity]

        # update the body translational direction and heading states as well
        self.state_direction = v_b
        self.state_heading = np.array([np.cos(theta), np.sin(theta)])
        world_angle = theta + np.arctan2(v_b[1], v_b[0])
        self.state_world_direction = np.array(
            [np.cos(world_angle), np.sin(world_angle)]
        )

    def epg_activity_profile(self, discretized_angles: np.ndarray, shape_param=1.0):
        """return a discretized activity profile (cos squared), representing bump 
           amplitude along a dimension

        Args:
            discretized_angles (np.ndarray): angles to sample the distribution at
            shape_param (float, optional): scaling factor. Defaults to 1.0.

        Returns:
            np.ndarray: sampled activation profile
        """

        return shape_param * (np.cos(0.5 * discretized_angles)) ** 2

    def align_profile(self, profile: np.ndarray, target: float):
        """align a profile with a peak at 0, with a target angle such that 
           the peak of the profile it centered on the target angle

        Args:
            profile (np.ndarray): discrete activity profile
            goal_angle (float): desired distribution center

        Returns:
            np.ndarray: shifted profile 
        """

        target = self.wrap_to_pi(target)
        n_points = len(profile)
        shift = int(np.round(target / (2 * np.pi) * n_points, 0))

        return np.roll(profile, shift)

    def wrap_to_pi(self, angle: float):
        """utility function to wrap angular values to the range [-pi, pi]

        Args:
            angle (float): input angle

        Returns:
            float: output angle 
        """

        while angle < -np.pi:
            angle = angle + 2 * np.pi

        while angle > np.pi:
            angle = angle - 2 * np.pi

        return angle

    #### VISUALIZATION FUNCTIONS ####
    def init_PB_activities_recording(self, title="PB regional activity evolution"):
        
        self.pBfig, self.pBax = plt.subplots(3)
        
        self.pBfig.suptitle(title)
        
        plt.subplots_adjust(hspace=1)
        
        self.pBbars = [
            ax.bar(
                range(len(self.state_PBL_activity)),
                self.state_PBL_activity if i == 0 else self.state_PBR_activity,
                width=0.5,
            )
            for i, ax in enumerate(self.pBax[:len(self.pBax)-1])
        ] # PB plots 
        
        for ax in self.pBax[:len(self.pBax)-1]: 
            ax.set_ylim(0, 1.5)
            
        # the axis labels
        self.pBax[0].set_xlabel("discretized PBL region")
        self.pBax[0].set_ylabel("bump amp")
        
        self.pBax[1].set_xlabel("discretized PBR region")
        self.pBax[1].set_ylabel("bump amp")
            
        # Plot displacement and heading in the last subplot
        (self.PBdisplacement_line,) = self.pBax[-1].plot(
            [], [], label="Displacement (body-centric)"
        )
        (self.PBheading_line,) = self.pBax[-1].plot(
            [], [], label="Heading (world-centric)", color="r"
        )
        (self.PBworld_displacement_line,) = self.pBax[-1].plot(
            [], [], label="Displacement (world-centric)"
        )
        self.pBax[-1].set_xlim(-1.5, 1.5)
        self.pBax[-1].set_ylim(-1.5, 1.5)
        self.pBax[-1].set_xlabel("X")
        self.pBax[-1].set_ylabel("Y")
        self.pBax[-1].legend(bbox_to_anchor=(1, 1))
        
        self.pBax[-1].set_aspect("equal", "box")
    
    def init_PFN_activities_recording(self, index: list, title = "Decoded PFN and hDeltaB activity evolution"):

        num_plots = len(index)

        self.fig, self.ax = plt.subplots(
            num_plots + 2, gridspec_kw={"height_ratios": [1] * (num_plots + 1) + [2]}
        )
        
        self.fig.suptitle(title)
        
        plt.subplots_adjust(hspace=2)

        # Plot activations in the first subplots
        self.bars = [
            ax.bar(
                self.preferred_orientations[i] * 180 / np.pi,
                self.state_PFN_activities[i],
                width=30,
            )
            for i, ax in enumerate(self.ax[: (len(self.ax) - 2)])
        ]  # PFN activity plots
        self.bars.append(
            self.ax[-2].bar(
                list(range(len(self.state_hDeltaB_activities))),
                self.state_hDeltaB_activities,
                width=1,
            )
        )  # hDeltaB activity plot

        for i, ax in enumerate(self.ax[: len(self.ax) - 2]):
            ax.set_ylim(0, 1)
            ax.set_xticks(self.preferred_orientations[i] * 180 / np.pi)

        self.ax[-2].set_xticks(range(len(self.state_hDeltaB_activities)))
        self.ax[-2].set_ylim(-2, 2)
        
        # axis labels
        self.ax[0].set_xlabel("PFNdl sensitive heading direction")
        self.ax[0].set_ylabel("amp")
        self.ax[1].set_xlabel("PFNdR sensitive heading direction")
        self.ax[1].set_ylabel("amp")
        self.ax[2].set_xlabel("PFNvL sensitive heading direction")
        self.ax[2].set_ylabel("amp")
        self.ax[3].set_xlabel("PFNvR sensitive heading direction")
        self.ax[3].set_ylabel("amp")
        self.ax[4].set_xlabel("HdeltaB population")
        self.ax[4].set_ylabel("amp")

        # Plot displacement and heading in the last subplot
        (self.displacement_line,) = self.ax[-1].plot(
            [], [], label="Displacement (body-centric)"
        )
        (self.heading_line,) = self.ax[-1].plot(
            [], [], label="Heading (world-centric)", color="r"
        )
        (self.world_displacement_line,) = self.ax[-1].plot(
            [], [], label="Displacement (world-centric)"
        )
        self.ax[-1].set_xlim(-1.5, 1.5)
        self.ax[-1].set_ylim(-1.5, 1.5)
        self.ax[-1].set_xlabel("X")
        self.ax[-1].set_ylabel("Y")
        self.ax[-1].legend(bbox_to_anchor=(1, 1))
        self.ax[-1].set_aspect("equal", "box")
        
        # handles, labels = self.pBax[-1].get_legend_handles_labels()
        # self.fig.legend(handles, labels, loc='center right', bbox_to_anchor=(0, 5), fancybox=True, shadow=True)

    def init_hDeltaB_activities_recording(self, title="Decoded hDeltaB activity evolution"):

        self.hDbfig1 = plt.figure(figsize=(8, 6))
        self.hDbfig2 = plt.figure(figsize=(8, 6))
        
        self.hDbfig1.suptitle(title)
        self.hDbfig2.suptitle(title)
        
    def log_state(self):

        self.frames.append(len(self.frames))
        self.hDbframes.append(len(self.hDbframes))
        self.PBL_log.append(copy.copy(self.state_PBL_activity))
        self.PBR_log.append(copy.copy(self.state_PBR_activity))
        self.pFN_activities_log.append(copy.copy(self.state_PFN_activities))
        self.direction_log.append(copy.copy(self.state_direction))
        self.heading_log.append(copy.copy(self.state_heading))
        self.world_direction_log.append(copy.copy(self.state_world_direction))
        self.hDeltaB_activities_log.append(
            copy.copy(self.state_hDeltaB_activities)
        )
        self.hDeltaB_vectors_log.append(copy.copy(self.state_hDeltaB_world_vectors))
        self.rearranged_hDeltaB_log.append(copy.copy(self.rearranged_hDeltaB))

    def update_PB_animation(self, frame):
                
        # PBL 
        for i, (barL, barR) in enumerate(zip(self.pBbars[0],self.pBbars[1])): 
            barL.set_height(self.PBL_log[frame][i])
            barR.set_height(self.PBR_log[frame][i])
    
        # Update displacement values plot
        direction = self.direction_log[frame]
        self.PBdisplacement_line.set_data([0, direction[0]], [0, direction[1]])

        heading = self.heading_log[frame]
        self.PBheading_line.set_data([0, heading[0]], [0, heading[1]])

        world_direction = self.world_direction_log[frame]
        self.PBworld_displacement_line.set_data(
            [0, world_direction[0]], [0, world_direction[1]]
        )
        
        return self.pBbars, self.PBdisplacement_line, self.PBheading_line

    def update_PFN_animation(self, frame):

        # update PFN activities
        for i, (thetas, activities) in enumerate(
            zip(self.preferred_orientations, self.pFN_activities_log[frame])
        ):
            bar = self.bars[i]
            for j, b in enumerate(bar):
                b.set_height(activities[j])

        # update hDeltaB activities
        for i, b in enumerate(self.bars[-1]):
            b.set_height(self.hDeltaB_activities_log[frame][i])

        # Update displacement values plot
        direction = self.direction_log[frame]
        self.displacement_line.set_data([0, direction[0]], [0, direction[1]])

        heading = self.heading_log[frame]
        self.heading_line.set_data([0, heading[0]], [0, heading[1]])

        world_direction = self.world_direction_log[frame]
        self.world_displacement_line.set_data(
            [0, world_direction[0]], [0, world_direction[1]]
        )

        return self.bars, self.displacement_line, self.heading_line

    def update_hDeltaB_animation1(self, frame):

        log_numpy = np.array(self.hDeltaB_vectors_log) * 10

        plt.cla()  # Clear the current plot
        start_point = np.array([0, 0])
        for vec in log_numpy[frame]:
            end_point = start_point + vec
            plt.plot(
                [start_point[0], end_point[0]], [start_point[1], end_point[1]], "b"
            )  # Plotting lines for each vector
            start_point = end_point
        overall_vector = -np.sum(log_numpy[frame], axis=0)
        plt.plot(
            [start_point[0], start_point[0] + overall_vector[0]],
            [start_point[1], start_point[1] + overall_vector[1]],
            "r",
        )  # Plotting overall vector
        plt.xlim(-10, 10)
        plt.ylim(-10, 10)
        plt.xlabel("X")
        plt.ylabel("Y")
        plt.title(f"Timestep {frame+1}")

    def update_hDeltaB_animation2(self, frame):

        log_numpy = np.array([vec/np.linalg.norm(np.array(vec)) for vec in self.hDeltaB_vectors_log]) * 5 # TODO normalize this

        world_dir_unit_vector = self.world_direction_log[frame] / np.linalg.norm(
            self.world_direction_log[frame]
        ) * 3
        
        heading_unit_vector = self.heading_log[frame] / np.linalg.norm(
            self.heading_log[frame]
        ) * 3
        
        body_direction_unit_vector = self.direction_log[frame] / np.linalg.norm(
            self.direction_log[frame]
        ) * 3
        
        estimated_world_dir_vector = np.array([np.cos(self.decoded_angle_log[frame]), np.sin(self.decoded_angle_log[frame])])
        estimated_world_dir_unit_vector = estimated_world_dir_vector / np.linalg.norm(estimated_world_dir_vector) * 3

        plt.cla()  # Clear the current plot
        start_point = np.array([0, 0])
        for vec in log_numpy[frame]:
            start_point = (vec / np.linalg.norm(vec)) * 3
            end_point = start_point + vec
            plt.plot(
                [start_point[0], end_point[0]], [start_point[1], end_point[1]], "b", linewidth=3
            )  # Plotting lines for each vector
        plt.plot([0, world_dir_unit_vector[0]], [0, world_dir_unit_vector[1]], "r", linewidth=3, label="Displacement (World-centric)")
        plt.plot([0, heading_unit_vector[0]], [0, heading_unit_vector[1]], "g", label="Heading (World-centric)")
        plt.plot([0, body_direction_unit_vector[0]], [0, body_direction_unit_vector[1]], "orange", label="Displacement (Body-centric)")
        plt.plot([0, estimated_world_dir_unit_vector[0]], [0, estimated_world_dir_unit_vector[1]],'b',linewidth=3, label="Estimated Displacement (World-centric)")
        plt.plot()
        plt.xlim(-10, 10)
        plt.ylim(-10, 10)
        plt.xlabel("X (arbitrary magnitude)")
        plt.ylabel("Y (arbitrary magnitude)")
        plt.legend()

    def show_PB_animations(self, interval=1000, path="./outputs/PB_animations.mp4"):
        
        # Initialize the animation
        ani = FuncAnimation(
            self.pBfig, self.update_PB_animation, frames=self.frames, interval=interval
        )
        f = path 
        writervideo = FFMpegWriter(fps=1000/interval) 
        ani.save(f, writer=writervideo)
        plt.show()        

    def show_PFN_animations(self, interval=1000, path="./outputs/PFN_animation.mp4"):

        # Initialize the animation
        self.anim = FuncAnimation(
            self.fig, self.update_PFN_animation, frames=self.frames, interval=interval
        )
        f = path 
        writervideo = FFMpegWriter(fps=1000/interval) 
        self.anim.save(f, writer=writervideo)
        plt.show()

    def show_hDeltaB_animation1(self, interval=1000):

        # Initialize the animation
        ani = FuncAnimation(
            self.hDbfig1,
            self.update_hDeltaB_animation1,
            frames=self.hDbframes,
            interval=interval,
        )
        plt.show()

    def show_hDeltaB_animation2(self, interval=1000, path="./outputs/hDeltaB_animation.mp4"):

        # Initialize the animation
        ani = FuncAnimation(
            self.hDbfig2,
            self.update_hDeltaB_animation2,
            frames=self.hDbframes,
            interval=interval,
        )
        f = path 
        writervideo = FFMpegWriter(fps=1000/interval) 
        ani.save(f, writer=writervideo)
        plt.show()

    def show_angle_evolutions(self):
        fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(10, 8), sharex=True)
        
        decoded_angle_plot = [self.wrap_to_pi(a)*180/np.pi for a in self.decoded_angle_log]
        gt_angle_plot = [self.wrap_to_pi(a)*180/np.pi for a in self.gt_angle_log]
        decoded_angle_error_plot = [self.wrap_to_pi(a)*180/np.pi for a in self.decoded_error_log]
        direction_plot = [self.wrap_to_pi(np.arctan2(a[1],a[0]))*180/np.pi for a in self.direction_log]
        heading_plot = [self.wrap_to_pi(np.arctan2(a[1],a[0]))*180/np.pi for a in self.heading_log]

        ax1.plot(decoded_angle_plot, label="Decoded Angle")
        ax1.plot(gt_angle_plot, label="Ground Truth Angle")
        ax1.plot(decoded_angle_error_plot, label="Angular Error")
        ax1.set_ylabel("Angle (degrees)")
        ax1.legend()
        ax1.grid(True)

        ax2.plot(direction_plot, label="Body-Centric Direction")
        ax2.plot(heading_plot, label="Heading")
        ax2.set_xlabel("Time (s)")
        ax2.set_ylabel("Angle (degrees)")
        ax2.legend()
        ax2.grid(True)
        
        fig.suptitle("Decoded vs Ground Truth Displacement Angle in World Frame")


        plt.show()
